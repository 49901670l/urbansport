package com.example.y2267872v.urbansport.LoginTest;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.y2267872v.urbansport.Login;
import com.example.y2267872v.urbansport.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;

@RunWith(AndroidJUnit4.class)
public class LoginTest {

    @Rule
    public ActivityTestRule rule = new ActivityTestRule(Login.class,true, false);

    @Test
    public void comprobarTextoLogin() throws Exception{
        rule.launchActivity(new Intent());

        onView(withText(R.string.loginText)).check(matches(isDisplayed()));
    }

    @Test
    public void comprobarBackground() throws Exception{
        rule.launchActivity(new Intent());

        onView(withId(R.drawable.log)).check(matches(isDisplayed()));
    }

    @Test
    public void inicioHome(){
        rule.launchActivity(new Intent());
        onView(withId(R.id.loginA)).perform(navigateTo(R.id.homeC));
    }

    @Test
    public void botonLoginClicable(){
        rule.launchActivity(new Intent());
        onView(withId(R.id.loginL)).check(matches(isClickable()));
    }

}