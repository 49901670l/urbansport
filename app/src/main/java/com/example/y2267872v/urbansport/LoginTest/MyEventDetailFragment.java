package com.example.y2267872v.urbansport.LoginTest;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.y2267872v.urbansport.Evento;
import com.example.y2267872v.urbansport.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A placeholder fragment containing a simple view.
 */
public class MyEventDetailFragment extends Fragment {
    View view;
    private TextInputEditText title;
    private TextInputEditText description;
    private TextInputEditText availablePeople;
    private TextInputEditText totalPeople;
    private ImageView url;
    private Button Update;
    private Button Remove;

    public MyEventDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_event_detail, container, false);
        Intent i = getActivity().getIntent();

        if (i != null) {
            Evento evento = (Evento) i.getSerializableExtra("eventos");
            if (evento != null) {
                updateUi(evento);
            }
        }
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void updateUi (Evento evento) {
        Log.d("Eventos", evento.toString());

        title = view.findViewById(R.id.TitleMyEventDetal);
        description = view.findViewById(R.id.DescriptionMyEventDetal);
        availablePeople = view.findViewById(R.id.AvailablePeopleMyEventDetal);
        totalPeople = view.findViewById(R.id.TotalPeopleMyEventDetal);
        url = view.findViewById(R.id.PhotoMyEventDetal);
        Update = view.findViewById(R.id.Update);
        Remove = view.findViewById(R.id.Remove);

        try {
            DatabaseReference reference = callData();
            button(evento, reference, Update, Remove);
        }
        catch (Exception e) {
        }

        title.setText(evento.getTitulo());
        description.setText(evento.getDescription());
        availablePeople.setText(Integer.toString(evento.getAvailablePeople()));
        totalPeople.setText(Integer.toString(evento.getTotalPeople()));

        if (evento.getUrl() == null) {
            evento.setUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsuIFEanaJbvzTzDBfDm956BX4DZhFYS9c5SX2wRqiRMsz6GnCTw");
        }

        Glide.with(getContext()).load(
                evento.getUrl()
        ).into(url);
    }

    public DatabaseReference callData () {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();
        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference eventos = uid.child("eventos");
        return eventos;
    }

    public void button (Evento evento, DatabaseReference eventos, Button Update, Button Remove){
        View.OnClickListener anadir = (View view) -> {
            switch (view.getId()){
                case R.id.Update:
                    update(evento, eventos);
                    break;

                case R.id.Remove:
                    eventos.child(evento.getUid()).removeValue();
                    Toast.makeText(getContext(), "Remove", Toast.LENGTH_SHORT).show();
            }
        };
        Update.setOnClickListener(anadir);
        Remove.setOnClickListener(anadir);

    }

    public void update (Evento evento, DatabaseReference eventos){
        Evento e = new Evento();
        e.setUid(evento.getUid());
        e.setTitulo(title.getText().toString());
        e.setDescription(description.getText().toString());
        e.setTotalPeople(Integer.parseInt(String.valueOf(totalPeople.getText())));
        e.setAvailablePeople(Integer.parseInt(String.valueOf(availablePeople.getText())));
        e.setDireccion(evento.getDireccion());
        e.setLatitud(evento.getLatitud());
        e.setLongitud(evento.getLongitud());
        e.setUrl(evento.getUrl());

        eventos.child(evento.getUid()).setValue(e);
        Toast.makeText(getContext(), "Update", Toast.LENGTH_SHORT).show();
    }
}
