package com.example.y2267872v.urbansport;

import java.io.Serializable;

public class Evento implements Serializable {

    private String Uid;
    private String Titulo;
    private String Description;
    private int AvailablePeople;
    private int TotalPeople;
    private String url;
    private String direccion;
    private String longitud;
    private String latitud;


    public Evento(){
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        Uid = uid;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public int getAvailablePeople() {
        return AvailablePeople;
    }

    public void setAvailablePeople(int availablePeople) {
        AvailablePeople = availablePeople;
    }

    public int getTotalPeople() {
        return TotalPeople;
    }

    public void setTotalPeople(int totalPeople) {
        TotalPeople = totalPeople;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }
}
