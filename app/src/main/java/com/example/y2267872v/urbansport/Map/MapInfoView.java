package com.example.y2267872v.urbansport.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.y2267872v.urbansport.Evento;
import com.example.y2267872v.urbansport.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

@SuppressLint("ValidFragment")
public class MapInfoView implements GoogleMap.InfoWindowAdapter  {
    private final Activity activity;
    private View view;

    @SuppressLint("ValidFragment")
    public MapInfoView(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getInfoContents(Marker marker) {
        View view = activity.getLayoutInflater().inflate(R.layout.map_info_view, null);
        Evento evento = (Evento) marker.getTag();

        ImageView mapPhoto = view.findViewById(R.id.mapPhoto);
        TextView mapTitel = view.findViewById(R.id.mapTitel);
        TextView mapDescription = view.findViewById(R.id.mapDescription);
        TextView mapVailablePeople = view.findViewById(R.id.mapVailablePeople);
        TextView mapTotalPeopleLis = view.findViewById(R.id.mapTotalPeopleLis);

        mapTitel.setText(evento.getTitulo());
        mapDescription.setText(evento.getDescription());
        mapVailablePeople.setText(Integer.toString(evento.getAvailablePeople()));
        mapTotalPeopleLis.setText(Integer.toString(evento.getTotalPeople()));
        Glide.with(view.getContext()).load(
                evento.getUrl()
        ).into(mapPhoto);
        return view;
    }
}
