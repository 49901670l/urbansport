package com.example.y2267872v.urbansport;

import com.stephentuso.welcome.BasicPage;
import com.stephentuso.welcome.TitlePage;
import com.stephentuso.welcome.WelcomeActivity;
import com.stephentuso.welcome.WelcomeConfiguration;

public class WelcomeScreen extends WelcomeActivity {
    @Override
    protected WelcomeConfiguration configuration() {
        return new WelcomeConfiguration.Builder(this)
                .defaultBackgroundColor(R.color.colorAccent)
                .page(new TitlePage(R.drawable.logo,
                        "Bienvenido a UrbanSport")
                )
                .page(new BasicPage(R.drawable.compa,
                        "Encuentra eventos",
                        "Puedes encontrar gente que busca completar su equipo de fútbol, basquet, o tal vez, salir a bailar en grupo.")
                        .background(R.color.zulo)
                )
                .swipeToDismiss(true)
                .build();
    }
}
