package com.example.y2267872v.urbansport.Home;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.y2267872v.urbansport.Evento;
import com.example.y2267872v.urbansport.Map.InfoViewMap;
import com.example.y2267872v.urbansport.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DetailEventFragment extends Fragment {

    private View view;

    public DetailEventFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_detail_event, container, false);
        Intent i = getActivity().getIntent();

        if (i != null) {
            Evento evento = (Evento) i.getSerializableExtra("eventos");
            if (evento != null) {
                updateUi(evento);
            }
        }
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void updateUi (Evento evento) {
        Log.d("Eventos", evento.toString());

        TextView title = view.findViewById(R.id.TitleEventDetal);
        TextView description = view.findViewById(R.id.DescriptionEventDetal);
        TextView availablePeople = view.findViewById(R.id.AvailablePeopleEventDetal);
        TextView totalPeople = view.findViewById(R.id.TotalPeopleEventDetal);
        ImageView url = view.findViewById(R.id.PhotoEventDetal);

        unirse(evento, availablePeople);

        title.setText(evento.getTitulo());
        description.setText(evento.getDescription());
        availablePeople.setText(Integer.toString(evento.getAvailablePeople()));
        totalPeople.setText(Integer.toString(evento.getTotalPeople()));

        /*if (evento.getUrl().contains(".mp4")){
            System.out.println("URL mp4: === "  + evento.getUrl());
            videoView.setVideoURI(Uri.parse(evento.getUrl()));
            MediaController mediaController = new MediaController(getContext());
            videoView.setMediaController(mediaController);
            mediaController.setAnchorView(videoView);
        }*/

        Glide.with(getContext()).load(
                evento.getUrl()
        ).into(url);
    }

    public void unirse(Evento evento, TextView availablePeople){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        try {
            System.out.println("Eventos 1  " + evento.getAvailablePeople());
            Button JoinEventDetal = view.findViewById(R.id.JoinEventDetal);
            Button map = view.findViewById(R.id.Map);

            @SuppressLint("SetTextI18n") View.OnClickListener anadir = (View view) -> {
                switch (view.getId()){
                    case R.id.JoinEventDetal:
                        DatabaseReference users = base.child("users");
                        DatabaseReference uid = users.child(auth.getUid());
                        DatabaseReference eventos = uid.child("eventos");

                        Evento e = new Evento();
                        e.setUid(evento.getUid());
                        e.setTitulo(evento.getTitulo());
                        e.setDescription(evento.getDescription());
                        e.setTotalPeople(evento.getTotalPeople());
                        if (e.getAvailablePeople() <= evento.getTotalPeople()){
                            e.setAvailablePeople(evento.getAvailablePeople() + 1);
                        }
                        else {
                            e.setAvailablePeople(evento.getAvailablePeople());
                            Toast.makeText(getContext(), "Evento Lleno", Toast.LENGTH_SHORT).show();
                        }
                        e.setDireccion(evento.getDireccion());
                        e.setLatitud(evento.getLatitud());
                        e.setLongitud(evento.getLongitud());
                        e.setUrl(evento.getUrl());

                        availablePeople.setText(Integer.toString(e.getAvailablePeople()));
                        eventos.child(evento.getUid()).setValue(e);
                        Toast.makeText(getContext(), "Unirse", Toast.LENGTH_SHORT).show();
                        break;


                    case R.id.Map:
                        Intent intent = new Intent(getActivity(), InfoViewMap.class);
                        startActivity(intent);
                        Toast.makeText(getContext(), "Mapa", Toast.LENGTH_SHORT).show();

                        break;
                }
            };
            JoinEventDetal.setOnClickListener(anadir);
            map.setOnClickListener(anadir);
        }
        catch (Exception e) {

        }
    }
}
