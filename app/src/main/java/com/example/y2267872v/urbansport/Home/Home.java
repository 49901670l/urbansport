package com.example.y2267872v.urbansport.Home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.example.y2267872v.urbansport.Evento;
import com.example.y2267872v.urbansport.R;
import com.example.y2267872v.urbansport.newEvent;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment {

    View view;
    MaterialButton participar;
    TextView username;
    TextView userEmail;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_menu, menu);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        participar = view.findViewById(R.id.participar);
        username = view.findViewById(R.id.UserName);
        userEmail = view.findViewById(R.id.emailUser);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();


        try {

            DatabaseReference users = base.child("users");
            DatabaseReference uid = users.child(auth.getUid());
            DatabaseReference eventos = uid.child("eventos");

            FirebaseListOptions<Evento> options = new FirebaseListOptions.Builder<Evento>()
                    .setQuery(eventos, Evento.class)
                    .setLayout(R.layout.listdata)
                    .setLifecycleOwner(this)
                    .build();

            FirebaseListAdapter<Evento> adapter = new FirebaseListAdapter<Evento>(options) {
                @SuppressLint("SetTextI18n")
                @Override
                protected void populateView(View v, Evento model, int position) {
                    TextView titleListDetal = v.findViewById(R.id.TitleListDetal);
                    TextView availablePeopleListData = v.findViewById(R.id.availablePeopleListData);
                    TextView totalPeopleListData = v.findViewById(R.id.totalPeopleListData);
                    ImageView url = v.findViewById(R.id.PhotoListDate);

                    titleListDetal.setText(model.getTitulo());
                    availablePeopleListData.setText(Integer.toString(model.getAvailablePeople()));
                    totalPeopleListData.setText(Integer.toString(model.getTotalPeople()));
                    Glide.with(getContext()).load(
                            model.getUrl()
                    ).into(url);
                }
            };
            ListView listItem = view.findViewById(R.id.ListIenm);
            listItem.setAdapter(adapter);

            listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Evento evento = (Evento) parent.getItemAtPosition(position);
                    Intent intent = new Intent(getContext(), DetailEvent.class);
                    intent.putExtra("eventos", evento);
                    startActivity(intent);
                }
            });

        }
        catch (Exception e){

        }
        return view;
    }
}