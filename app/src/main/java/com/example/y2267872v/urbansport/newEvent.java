package com.example.y2267872v.urbansport;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 */
public class newEvent extends Fragment {

    ProgressBar mLoading;
    private TextInputEditText txtLatitud;
    private TextInputEditText txtLongitud;
    private TextInputEditText txtDireccio;
    private TextInputEditText titulo;
    private TextInputEditText description;
    private TextInputEditText availablePeople;
    private TextInputEditText totalPeople;

    String mCurrentPhotoPath;
    private Uri photoURI;
    private ImageView foto;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int IMAGE_GALLERY_REQUEST = 1;

    private Button upload;
    private Button Video;
    private Button Photo;
    private Button Gallery;
    private SharedViewModel model;
    private StorageReference sReference;
    private String downloadUrl;
    private View view;

    public newEvent() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.new_event, container, false);

        asignarCampos();

        localizacion();

        Gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakeGalleryIntent();
            }
        });

        Photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        Video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakeVideoIntent();
            }
        });


        upload.setOnClickListener(button -> {
            FirebaseStorage storage = FirebaseStorage.getInstance();
            sReference = storage.getReference();
            StorageReference imageRef = sReference.child(mCurrentPhotoPath);

            UploadTask uploadTask = imageRef.putFile(photoURI);
            uploadTask.addOnSuccessListener(taskSnapshot -> {
                taskSnapshot.getMetadata();
                imageRef.getDownloadUrl().addOnCompleteListener(task -> {
                    DatabaseReference eventos = callData();

                    Uri dwUri = task.getResult();
                    Glide.with(this).load(dwUri).into(foto);
                    downloadUrl = dwUri.toString();

                    guardarDatos(eventos);
                });
            });
        });
        return view;
    }

    private DatabaseReference callData () {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();
        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference eventos = uid.child("eventos");
        return eventos;
    }

    private void asignarCampos () {
        mLoading = view.findViewById(R.id.loading);
        txtLatitud =  view.findViewById(R.id.latS);
        txtLongitud = view.findViewById(R.id.longS);
        txtDireccio = view.findViewById(R.id.direccionS);
        titulo = view.findViewById(R.id.Titulo);
        description = view.findViewById(R.id.Description);
        availablePeople = view.findViewById(R.id.AvailablePeople);
        totalPeople = view.findViewById(R.id.TotalPeople);
        upload = view.findViewById(R.id.Upload);
        Video = view.findViewById(R.id.Video);
        Photo = view.findViewById(R.id.Photo);
        Gallery = view.findViewById(R.id.Gallery);
        foto = view.findViewById(R.id.cameraSe);

    }

    private void localizacion () {
        model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        model.getCurrentAddress().observe(this, address -> {
            txtDireccio.setText(getString(R.string.address_text,
                    address, System.currentTimeMillis()));
        });

        model.getCurrentLatLng().observe(this, latlng -> {
            txtLatitud.setText(String.valueOf(latlng.latitude));
            txtLongitud.setText(String.valueOf(latlng.longitude));
        });

        model.getProgressBar().observe(this, visible -> {
            if(visible)
                mLoading.setVisibility(ProgressBar.VISIBLE);
            else
                mLoading.setVisibility(ProgressBar.INVISIBLE);
        });

        model.switchTrackingLocation();
    }

    private void guardarDatos (DatabaseReference eventos) {
        Evento newEvent =  new Evento();
        newEvent.setUid(UUID.randomUUID().toString());
        newEvent.setTitulo(titulo.getText().toString());
        newEvent.setDescription(description.getText().toString());
        newEvent.setTotalPeople(Integer.parseInt(String.valueOf(totalPeople.getText())));
        newEvent.setAvailablePeople(Integer.parseInt(String.valueOf(availablePeople.getText())));
        newEvent.setLatitud(txtLatitud.getText().toString());
        newEvent.setLongitud(txtLongitud.getText().toString());
        newEvent.setDireccion(txtDireccio.getText().toString());
        newEvent.setUrl(downloadUrl);

        DatabaseReference reference = eventos.child(newEvent.getUid());
        reference.setValue(newEvent);
        Toast.makeText(getContext(), "Aviso enviado", Toast.LENGTH_SHORT).show();
    }

    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(
                getContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                Toast.makeText(getContext(), "You have taken a picture!!!", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void dispatchTakeGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMAGE_GALLERY_REQUEST);
                Toast.makeText(getContext(), "GALLERY!!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File createVideoFile() throws IOException {
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageVideoName = "VIDEO_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File video = File.createTempFile(
                imageVideoName,
                ".mp4",
                storageDir
        );
        mCurrentPhotoPath = video.getAbsolutePath();
        return video;
    }
    private void dispatchTakeVideoIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takePictureIntent.resolveActivity(
                getContext().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createVideoFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(getContext(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
                Toast.makeText(getContext(), "You made a video!!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_TAKE_PHOTO) {
            Glide.with(this).load(photoURI).into(foto);
        }
        else {
            Toast.makeText(getContext(), "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
        }

        if (requestCode == IMAGE_GALLERY_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            photoURI = data.getData();
            Glide.with(this).load(photoURI).into(foto);
        }
        else {
            Toast.makeText(getContext(), "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
        }
    }
}
