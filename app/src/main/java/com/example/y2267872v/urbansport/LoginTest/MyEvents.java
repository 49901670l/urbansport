package com.example.y2267872v.urbansport.LoginTest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.y2267872v.urbansport.Evento;
import com.example.y2267872v.urbansport.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyEvents extends Fragment {

    public MyEvents() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_events, container, false);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        try {
            DatabaseReference users = base.child("users");
            DatabaseReference uid = users.child(auth.getUid());
            DatabaseReference eventos = uid.child("eventos");

            FirebaseListOptions<Evento> options = new FirebaseListOptions.Builder<Evento>()
                    .setQuery(eventos, Evento.class)
                    .setLayout(R.layout.listdata)
                    .setLifecycleOwner(this)
                    .build();

            FirebaseListAdapter<Evento> adapter = new FirebaseListAdapter<Evento>(options) {
                @Override
                protected void populateView(View v, Evento model, int position) {
                    TextView titleListDetal = v.findViewById(R.id.TitleListDetal);
                    TextView availablePeopleListData = v.findViewById(R.id.availablePeopleListData);
                    TextView totalPeopleListData = v.findViewById(R.id.totalPeopleListData);
                    ImageView url = v.findViewById(R.id.PhotoListDate);

                    titleListDetal.setText(model.getTitulo());
                    availablePeopleListData.setText(Integer.toString(model.getAvailablePeople()));
                    totalPeopleListData.setText(Integer.toString(model.getTotalPeople()));

                    if (model.getUrl() == null) {
                        model.setUrl("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsuIFEanaJbvzTzDBfDm956BX4DZhFYS9c5SX2wRqiRMsz6GnCTw");
                    }

                    Glide.with(getContext()).load(
                            model.getUrl()
                    ).into(url);
                }
            };
            ListView listItem = view.findViewById(R.id.listVente);
            listItem.setAdapter(adapter);

            listItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Evento evento = (Evento) parent.getItemAtPosition(position);
                    Intent intent = new Intent(getContext(), MyEventDetail.class);
                    intent.putExtra("eventos", evento);
                    startActivity(intent);
                }
            });
        }
        catch (Exception e){

        }
        return view;
    }
}
