package com.example.y2267872v.urbansport.Map;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.y2267872v.urbansport.Evento;
import com.example.y2267872v.urbansport.R;
import com.example.y2267872v.urbansport.SharedViewModel;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment {

    private GoogleMap.OnMapClickListener mCustomOnMapClickListener;

    public MapaFragment() {
        // Required empty public constructor
    }


    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mapa, container, false);

        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.g_map);

            FirebaseAuth auth = FirebaseAuth.getInstance();
            DatabaseReference base = FirebaseDatabase.getInstance().getReference();

            DatabaseReference users = base.child("users");
            DatabaseReference uid = users.child(auth.getUid());
            DatabaseReference eventos = uid.child("eventos");

            SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

            mapFragment.getMapAsync(map -> {
                map.setMyLocationEnabled(true);

                MutableLiveData<LatLng> currentLatLng = model.getCurrentLatLng();

                LifecycleOwner owner = getViewLifecycleOwner();
                currentLatLng.observe(owner, latLng -> {
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                    map.animateCamera(cameraUpdate);
                    currentLatLng.removeObservers(owner);
                });

                MapInfoView mapInfoView = new MapInfoView(
                  getActivity()
                );

                eventos.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Evento evento = dataSnapshot.getValue(Evento.class);

                        LatLng aux = new LatLng(
                                Double.valueOf(evento.getLatitud()),
                                Double.valueOf(evento.getLongitud())
                        );

                        Marker marker = map.addMarker(new MarkerOptions()
                                .title(evento.getTitulo())
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                .position(aux));
                        marker.setTag(evento);
                        map.setInfoWindowAdapter(mapInfoView);
                        try {
                            // Customize the styling of the base map using a JSON object defined
                            // in a raw resource file.
                            boolean success = map.setMapStyle(
                                    MapStyleOptions.loadRawResourceStyle(
                                            getActivity(), R.raw.map_style));

                            if (!success) {
                                Log.e(null, "Style parsing failed.");
                            }
                        } catch (Resources.NotFoundException e) {
                            Log.e(null, "Can't find style. Error: ", e);
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            });
        }
        catch (Exception e){

        }
        return view;
    }
}
