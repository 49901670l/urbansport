package com.example.y2267872v.urbansport;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nikartm.button.FitButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.stephentuso.welcome.WelcomeHelper;

public class Register extends AppCompatActivity {

    private TextInputEditText email;
    private EditText password;
    private FitButton register;

    FirebaseDatabase database;
    DatabaseReference users;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        getSupportActionBar().hide();

        database = FirebaseDatabase.getInstance();
        users = database.getReference("Users");

        email = findViewById(R.id.emailR);
        password = findViewById(R.id.passwordR);
        register = findViewById(R.id.registerR);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = email.getText().toString().trim();
                String pass  = password.getText().toString().trim();

                if(TextUtils.isEmpty(username)){
                    Toast.makeText(getApplicationContext(), "Se debe ingresar un email", Toast.LENGTH_LONG).show();
                    return;
                }

                if(TextUtils.isEmpty(pass)){
                    Toast.makeText(getApplicationContext(), "Falta ingresar la contraseña", Toast.LENGTH_LONG).show();
                    return;
                }

                FirebaseAuth auth = FirebaseAuth.getInstance();

                auth.createUserWithEmailAndPassword(username, pass)
                        .addOnCompleteListener(Register.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    Toast.makeText(getApplicationContext(), "Se ha registrado con éxito", Toast.LENGTH_LONG).show();

                                    Intent home = new Intent(getApplication(), MainActivity.class);
                                    startActivity(home);
                                }else {
                                    Toast.makeText(getApplicationContext(), "Fallo en el login", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

            }
        });
    }
}
